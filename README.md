This repo contains the build_lofar.sh script and some associated config
to help building the LOFAR software stack on HPC systems (or any other
(Linux)) box. Look at the script's output when called without config
to usage hints, and also at the example config provided.