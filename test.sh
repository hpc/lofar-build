print_init()
{
  cat <<EOT
# LOFAR Tools built by $USER@$(hostname) on $(date)
#
# Sourcing this script shall re-create the build environment and
# add things from the LOFAR prefix to appropriate path variables.
# It does _not_ set LD_LIBRARY_PATH (unless the build environment
# does), as a proper build should use RPATH in the binaries.
# But there is LD_RUN_PATH / LIBRARY_PATH set for link-time usage.
# You can set LD_LIBRARY_PATH=\$LD_RUN_PATH as a hack if some binary
# fails to find its libs. But this would be a bug in the build
# script that should be fixed properly.
#
# Step 1: The build config with all variables used.
#

lofar_prefix='$lofar_prefix'

EOT
  cat $config
cat <<EOT
#
# Step 2: Paths to LOFAR tools in the prefix.
#
EOT
  for p in lsmtool pyBDSF wsclean dysco DP3\
         idg aoflagger python-casacore casacore
  do
    echo "# $p"
    pp="$lofar_prefix/$p"
    test -d "$pp/bin" &&
    echo "export PATH=\"\$lofar_prefix/$p/bin:\$PATH\""
    test -d "$pp/lib" &&
      cat <<EOT
export LD_RUN_PATH=\"\$lofar_prefix/$p/lib:\$LD_RUN_PATH\"
export LIBRARY_PATH=\"\$lofar_prefix/$p/lib:\$LIBRARY_PATH\"
EOT
    test -d "$pp/include" &&
      echo "export CPATH=\"\$lofar_prefix/$p/include:\$CPATH\""
    pylib="$p/lib/python$pyver/site-packages"
    test -d "$lofar_prefix/$pylib" &&
      	echo "export PYTHONPATH=\"\$lofar_prefix/$pylib:\$PYTHONPATH\""
  done
}

print_init > "$lofar_prefix/init.sh" &&
echo "Finished installation. You can source $lofar_prefix/init.sh and run."
true
